
from pygraph.graph import*


def roof(x, y, w, h):
    brushColor("firebrick")
    polygon([(x, y), (x+w/2, y+h), (x-w/2, y+h)])
    


def wall(x, y, w, h):
    brushColor("grey")
    penSize(5)
    rectangle(x, y, x+w, y+h)


def rwindow(x, y, r):
    #x, y, r = 605, 215, 35
    circle(x, y, r)
    line(x-r, y, x+r, y)
    line(x, y-r, x, y+r)


def custik(custik_x,custik_y,custik_r,custik_x2,custik_y2,custik_r2, custik_x3,  custik_y3, custik_r3 ):
     brushColor("green")
     circle(custik_x, custik_y, custik_r)
     circle(custik_x2,custik_y2,custik_r2)
     circle( custik_x3, custik_y3,  custik_r3)
     
def chimney(x,y,w,h):   
    
 rectangle(x, y, x+w, y+h)


def door(x, y, w, h):
    brushColor("black")
    rectangle(y, x, w+x, y+h)


def window(x, y, w, h):
    brushColor("blue")
    rectangle(x, y, x+w, y+h)
    # vertical
    line(x+w/2, y, x+w/2, y+h)
    # horizontal
    line(x, y+h/2, x+w, y+h/2)


def bush():
    pass



def house(x, y, h, w):
    wall_h = h/3*2
    wall_w = w
    wall_x = x
    wall_y = y+h/3
    wall(x=wall_x, y=wall_y, w=wall_w, h=wall_h)









    chimney_w = wall_w/9
    chimney_h = wall_h/3
    chimney_x = wall_x + w/3
    chimney_y = y - chimney_h/2
    chimney(chimney_x, chimney_y, chimney_w, chimney_h, )

    roof_x = x+w/2
    roof_y = y
    roof_w = w
    roof_h = h/3
    roof(x=roof_x, y=roof_y, w=roof_w, h=roof_h)

    door_h = wall_h/2
    door_w = wall_w/6
    door_y = wall_h + wall_y - door_h
    door_x = wall_x + wall_w/2 - door_w/2
    door(door_x, door_y, door_w,  door_h)

    
    window_w = wall_h/2.2
    window_h = window_w
    lwindow_x = wall_x + wall_w/10
    window_y = wall_y + wall_h/6
    rwindow_x = wall_x + wall_w - wall_w/10 - window_w
    window(lwindow_x, window_y, window_w, window_h)
    window(rwindow_x, window_y, window_w, window_h)
    rwindow(wall_x+wall_w/2, roof_y+roof_h/2, roof_h/5)
    
    custik_x=wall_x-wall_w/6
    custik_y=wall_y+wall_h/1.25
    custik_r=wall_h/5
    custik_x2=custik_x/3
    custik_y2=custik_y+25
    custik_r2=custik_r/1.5
    custik_x3=custik_x2*5
    custik_y3=custik_y2
    custik_r3=custik_r2
    custik(custik_x, custik_y,custik_r,custik_x2,custik_y2,custik_r2, custik_x3, custik_y3, custik_r3)



if __name__ == "__main__":
    canvas_w = 1200
    canvas_h = 800
    windowSize(canvas_w, canvas_h)
    canvasSize(canvas_w, canvas_h)

    house(x=canvas_w/5, y=canvas_h/3, w=800, h=500)

    run()
